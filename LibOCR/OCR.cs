﻿using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Media.Ocr;
using Windows.Storage.Streams;

namespace LibOCR
{
    /// <summary>
    /// OCR engine
    /// </summary>
    public class OCR
    {
        public OcrEngine MyOcrEngine { get; private set; }
        public OCR()
        {
            MyOcrEngine = OcrEngine.TryCreateFromUserProfileLanguages();
        }

        /// <summary>
        /// Convert Image in byte format to SoftwareBitmap
        /// </summary>
        /// <param name="Image">Imgae in byte format</param>
        /// <returns>Image in SoftwareBitmap</returns>
        public static async Task<SoftwareBitmap> GetPictureFromStreamAsync(byte[] Image)
        {
            SoftwareBitmap picture = null;

            using (InMemoryRandomAccessStream randomAccessStream = new InMemoryRandomAccessStream())
            {
                await randomAccessStream.WriteAsync(Image.AsBuffer());
                randomAccessStream.Seek(0);
                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(randomAccessStream);
                picture = await decoder.GetSoftwareBitmapAsync();
            }

            return picture;
        }

        /// <summary>
        /// Decode MemoryStream image with OCR UWP
        /// </summary>
        /// <param name="Image">Memory to decode</param>
        /// <returns>string decoded</returns>
        public async Task<string> DecodePictureFromStreamAsync(MemoryStream Image)
        {
            SoftwareBitmap Picture = await GetPictureFromStreamAsync(Image.GetBuffer());
            OcrResult ResultOCR = await MyOcrEngine.RecognizeAsync(Picture);
            return ResultOCR.Text;
        }
    }
}
