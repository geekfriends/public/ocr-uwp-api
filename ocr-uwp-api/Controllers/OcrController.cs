﻿using LibOCR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ocr_uwp_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OcrController : ControllerBase
    {
        /// <summary>
        /// GET api/Ocr
        /// </summary>
        /// <returns>return test value</returns>
        [HttpGet]
        public string Get()
        {
            return "Get value";
        }

        /// <summary>
        /// POST api/Ocr
        /// </summary>
        /// <param name="UploadFile">File to analyse</param>
        /// <returns>Result of OCR</returns>
        [HttpPost]
        public async Task<string> PostOCRAsync([FromForm]IFormFile UploadFile)
        {
            string MyText = "error";
            MemoryStream file = null;

            if (UploadFile == null)
                return "File is missing";

            using (file = new MemoryStream())
            {
                UploadFile.CopyTo(file);
            }

            try
            {
                OCR MyEngine = new OCR();
                MyText = await MyEngine.DecodePictureFromStreamAsync(file);
            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
            }

            return MyText;
        }
    }
}
